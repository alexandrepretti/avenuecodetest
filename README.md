##**Alexandre Pretti - Avenue Code Test - QA**

All Bugs and Enhacements is on the tracking tool on the website : https://qa-test.avenuecode.com/bugs

I used the login: **apretti93@hotmail.com** and the password **12345678**

---

## Automation

The script is the */AvenueCodeAutomation/* folder

I used Eclipse IDE with Java, Selenium and Cucumber

How to run the script:

1. Open Eclipse IDE
2. Click on "Import existing projects"
3. Click on Select archive file and Browse to the Folder
4. Select ScriptAutomationAvenueCode.zip file then click Finish
5. On Package Explorer right click on "teste" > Run As > JUnit Test

---

## JMeter Script

The script is the */ScriptStress/* folder

I used JMeter. 
There is a .jmx file (the script that only add an subtask on a specific task)
a .jtl (results) and a folder report with an index.html (with graphs and other stuffs about the results)


---

## Other Tests and Comments

There is an PDF file called "OthersTestsComments.pdf". 
First i comment (in Portugueses) all my obstacles; what i think during this tests, etc;

After i put some other tests

