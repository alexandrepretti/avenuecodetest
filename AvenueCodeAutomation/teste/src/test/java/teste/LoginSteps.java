package teste;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import teste.LoginPage;

public class LoginSteps {
	@Given("that i access the ToDo App Website")
	public void acessWebsite() {

	}

	@When("I put a valid user")
	public void userInput() {
		LoginPage loginPage = new LoginPage();
		loginPage.inputLogin();
	}

	@And("i put a valid password")
	public void passwordInput() {
		LoginPage loginPage = new LoginPage();
		loginPage.inputPassword();

	}

	@And("click on Sign in button")
	public void signInClick() {
		LoginPage loginPage = new LoginPage();
		loginPage.clickSign();
	}

	@Then("check sign in message")
	public void checkMessage() {
		LoginPage loginPage = new LoginPage();
		loginPage.checkMessage();

	}
	
	@And ("i log out")
	public void logoutClick() {
		LoginPage loginPage = new LoginPage();
		loginPage.clickLogout();
	}
	
	
}
