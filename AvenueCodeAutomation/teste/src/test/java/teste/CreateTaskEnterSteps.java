package teste;

import cucumber.api.java.en.And;


public class CreateTaskEnterSteps {

	@And("i put a different valid entry")
	public void sendOtherEntry() {
		TaskPage taskPage = new TaskPage();
		taskPage.sendDifferentEntry();
	}
	
	@And("I press Enter")
	public void addClick() {
		TaskPage taskPage = new TaskPage();
		taskPage.pressEnterKeyboard();
	}
	

}
