package teste;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginElementMap {
	protected WebElement user_email;
	protected WebElement user_password;
	protected WebElement commit;
	
	@FindBy(xpath = "/html/body/div[1]/div[2]")
	protected WebElement welcomeMessage;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div/div[2]/ul[2]/li[2]/a")
	protected WebElement logoutButton;
		
}
