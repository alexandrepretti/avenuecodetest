package teste;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateTaskSteps {

	@Given("that im logged")
	public void loginComplete() {
		TaskPage taskPage = new TaskPage();
		taskPage.loginSuccess();

	}
	
	@When("I click on My Tasks")
	public void clickMyTasks() {
		TaskPage taskPage = new TaskPage();
		taskPage.clickMyTasks();
	}
	
	@And("I click on Task name field")
	public void clickTaskField() {
		TaskPage taskPage = new TaskPage();
		taskPage.clickTask();
	}

	@And("i put a valid entry")
	public void enterTaskName() {
		TaskPage taskPage = new TaskPage();
		taskPage.sendName();
	}

	@And("click on add button")
	public void addClick() {
		TaskPage taskPage = new TaskPage();
		taskPage.clickAdd();
	}

	@Then("my task is created")
	public void checkTaskCreated() {
		TaskPage taskPage = new TaskPage();
		taskPage.checkTask();
	}

}
