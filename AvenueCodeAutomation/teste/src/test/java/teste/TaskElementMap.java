package teste;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class TaskElementMap {
	protected WebElement user_email;
	protected WebElement user_password;
	protected WebElement commit;
	protected WebElement new_task;
	
	@FindBy(xpath = "/html/body/div[1]/div[2]/div[1]/form/div[2]/span")
	protected WebElement add_button;
	
	@FindBy(xpath = "/html/body/div[1]/div[3]/center/a")
	protected WebElement my_tasks;
	
	@FindBy(xpath="/html/body/div[1]/div[2]/div[2]/div")
	protected WebElement task_modal;
	
	@FindBy(xpath="/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[1]/td[5]/button")
	protected WebElement first_task;
	
	@FindBy(xpath="/html/body/div[1]/div[2]/div[2]/div/table/tbody/tr[2]/td[5]/button")
	protected WebElement second_task;

}
