package teste;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.PageFactory;

public class TaskPage extends TaskElementMap {
	
	public TaskPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void loginSuccess() {
		user_email.sendKeys("apretti93@hotmail.com");
		user_password.sendKeys("12345678");
		commit.click();
	}

	public void clickTask() {
		new_task.click();
	}

	public void sendName() {
		new_task.sendKeys("Example");	
	}


	public void clickAdd() {
		add_button.click();		
	}
	
	public void checkTask() {
	}

	public void clickMyTasks() {
		my_tasks.click();
	}

	public void pressEnterKeyboard() {
		new_task.sendKeys(Keys.ENTER);
	}

	public void sendDifferentEntry() {
		new_task.sendKeys("Enter Example");
		
	}

	public void deleteFirstTask() {
		first_task.click();
	}
	
	public void deleteSecondTask() {
		second_task.click();
	}


}
