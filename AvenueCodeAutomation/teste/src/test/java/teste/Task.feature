#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Manage Tasks

  @tag1
  Scenario: Create Task with click
    Given that im logged
    When I click on My Tasks
    And I click on Task name field
    And i put a valid entry
    And click on add button
    Then my task is created

  @tag2
  Scenario: Create Task pressing Enter
    Given that im logged
    When I click on My Tasks
    And I click on Task name field
    And i put a different valid entry
    And I press Enter
    Then my task is created
 
  @tag3
  Scenario: Delete the two previous Tasks
    Given that im logged
    When I click on My Tasks
    And I click on delete first task
    And I click on delete second task
    Then my task is deleted
    