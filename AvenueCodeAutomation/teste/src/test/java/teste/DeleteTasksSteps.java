package teste;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;


public class DeleteTasksSteps {

	@And("I click on delete first task")
	public void deleteFirstTask() {
		TaskPage taskPage = new TaskPage();
		taskPage.deleteFirstTask();
	}
	
	@And("I click on delete second task")
	public void deleteSecondTask() {
		TaskPage taskPage = new TaskPage();
		taskPage.deleteSecondTask();
	}
	
	@Then("my task is deleted")
	public void checkDelete() {
		TaskPage taskPage = new TaskPage();
		taskPage.checkTask();
	}
	

}
