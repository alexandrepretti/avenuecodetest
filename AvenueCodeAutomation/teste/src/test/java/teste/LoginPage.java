package teste;

import org.openqa.selenium.support.PageFactory;

public class LoginPage extends LoginElementMap {

	public LoginPage() {
		PageFactory.initElements(TestRule.getDriver(), this);
	}

	public void inputLogin() {
		user_email.sendKeys("apretti93@hotmail.com");
	}

	public void inputPassword() {
		user_password.sendKeys("12345678");

	}

	public void clickSign() {
		commit.click();
	}


	public void checkMessage() {
		welcomeMessage.isDisplayed();		
	}


	public void clickLogout()  {
		logoutButton.click();
	}


}
