package teste;

import java.util.concurrent.TimeUnit;

import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.chrome.ChromeDriver;
//import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;

public class TestRule {

	private static WebDriver driver;

	@BeforeClass
	public static void beforeClass() {
		WebDriverManager.chromedriver().setup();
	}

	@Before
	public void beforeScenario() {
		
		 //System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver/chromedriver.exe");
		 //ChromeOptions options = new ChromeOptions();
		 //options.addArguments("user-data-dir=C:\\Users\\Pretti\\AppData\\Local\\Google\\Chrome\\User Data"); 
		 //driver = new ChromeDriver();
		 //driver.manage().window().maximize();
		 //driver.navigate().to("https://qa-test.avenuecode.com/users/sign_in");
		 
		System.setProperty("webdriver.gecko.driver", "src/test/resources/chromedriver/geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(5,  TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get("https://qa-test.avenuecode.com/users/sign_in");
	}

	@After
	public void teardown() {
		if (driver != null) {
			driver.quit();
		}
	}

	public static WebDriver getDriver() {
		return driver;
	}

}
